#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

/* you can find the required header files in the man pages. ie man getpid, man 2 fork  */
int main (int argc, char *argv[]) 
{
    printf("heyy kernel im here %d\n", (int) getpid());

    pid_t pid = fork();
    printf("forked %d\n", (int) pid);
/* we stop the program for a while to check the programs running at the terminal with ps -a
*/
	sleep (10);
    return 0;
}
